package demo.abstractcomponents;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	public WebDriver initializebrowser()
	{
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver(); // object of chromedriver
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(200));
		driver.manage().window().maximize();
		return driver;
	}


	/* And call SQL from file a.sql from IpgMDbTest3 database

	-> @When("call SQL from ...")
		databaseStepLibrary.callSqlFromFile(filePath, dbName);
			-> String sql = DtaL


	*/

}
