package cscart.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import demo.abstractcomponents.BaseClass;

public class CheckoutPage extends BaseClass{

	WebDriver driver;
	
	
	public CheckoutPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		//super(driver);
		this.driver = driver;  // initialization
		PageFactory.initElements(driver, this);
	}

	

	@FindBy(id ="litecheckout_s_address")
	WebElement customeraddress;
	@FindBy(id="litecheckout_email")
	WebElement customeremail;
	@FindBy(id="payments_2")
	WebElement payment;
	@FindBy(id ="customer_phone")
	WebElement customerphone;
	@FindBy(xpath ="//input[@name='accept_terms']")
	WebElement checkbox;
	@FindBy(xpath="//button[@name='dispatch[checkout.place_order]']")
	WebElement placeorder;
	
	
	public void fillcustomerdetails(String address, String email)
	{
		
		customeraddress.sendKeys(address);
		customeremail.sendKeys(email);
		
	}
	public void selectpayment(String phonenumber)
	{
	payment.click();
	customerphone.sendKeys(phonenumber);
	}
	
	
	public void placeorders() 
	{
		
		  checkbox.click();
		  driver.switchTo().frame(0); 
		  placeorder.click();
		 
		
}
	
	
}

	
