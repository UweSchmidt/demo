package cscart.demo;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import demo.abstractcomponents.BaseClass;

public class LandingPage extends BaseClass{

	WebDriver driver;
	
	
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
	//	super(driver);
		this.driver = driver;  // initialization
		PageFactory.initElements(driver, this);
	}

	//WebElement loginpage=driver.findElement(By.xpath("//li[@id='exist_demo']"));
	
	
	@FindBy(xpath ="//li[@id='exist_demo']")
	WebElement loginpage;
	@FindBy(id="email_existing")
	WebElement emailele;
	@FindBy(xpath="//button[contains(@_onclick, 'send')]")
	WebElement submit;
	
	public void loginDemo(String email)
	{
		loginpage.click();
		emailele.sendKeys(email);
		submit.click();
	}
	public void goTo()
	{
		driver.get("https://demo.cs-cart.com");
	}
}
