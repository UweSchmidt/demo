package cscart.demo;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import demo.abstractcomponents.BaseClass;

public class SearchProductsPage extends BaseClass{
	
	WebDriver driver;
	
	
	
	public SearchProductsPage(WebDriver driver) {
		//super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	//AbstractComponent ab = new AbstractComponent (driver);
	
	@FindBy(xpath="//form[@name ='search_form']")
	WebElement searchstart;
	@FindBy(id="search_input")
	WebElement searchinput;
	@FindBy(xpath="//button[@title='Search']")
	WebElement submitsearch;
	@FindBy (xpath="//div[@class='ut2-gl__name']")
	List<WebElement> productsby;
	@FindBy(xpath="//span[text()='Buy now']")
	WebElement addcart;
	@FindBy(id="cart_status_194")
	WebElement cartbutton;
	@FindBy(xpath="//span[text()='Checkout']")
	WebElement checkoutbutton;
	@FindBy(id="bp_off_bottom_panel")
	WebElement toolbar;
	@FindBy(xpath="//span[@id='products_search_total_found_66']")
	WebElement totalpageproducts;
	@FindBy(xpath="//span[@class='ty-float-right']")
	WebElement totalcartproducts;
	@FindBy(id="ranges_70_18")
	WebElement brandsec;
	@FindBy(xpath="//ul[@id='ranges_70_18']//li")
	List<WebElement> brandlist;
	
	
	
	
	public void searchproducts(String enterinput) throws InterruptedException
	{
		toolbar.click();
		searchstart.click();
		searchinput.sendKeys(enterinput);
		submitsearch.click();
		
		}
	
	public List<WebElement> getProductsList()
	{
	return productsby;
	
	}
	
	public void addAllProductsToCart() throws InterruptedException
	{
		
	//	ab.scrolldown(driver);
		for (int i = 0; i < productsby.size(); i++) {
			
		addcart.click();
		Thread.sleep(7000);
	}
		
	}
	
	public boolean verifycountofproducts() {
		
		String bss =totalpageproducts.getText();
		cartbutton.click();
		String a = totalcartproducts.getText();
		System.out.println(a);
		String [] arr2 = a.split(" ");
		String final2 = arr2[0];
		System.out.println(final2);
		boolean verify = bss.contains(final2);
		System.out.println(verify);
	    return verify;
	
	}
	public void clickcheckout()
	{
	
		checkoutbutton.click();
	}
	
	public void selectbrand(String brandname)
	{
		for(int i =0; i<brandlist.size();i++)
		{
			String a=brandlist.get(i).getText();
			if(a.equalsIgnoreCase(brandname)) {
				brandlist.get(i).click();
			}			
		}
	}	
}



