package cscart.demo;

import java.awt.Desktop.Action;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import demo.abstractcomponents.BaseClass;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AppDemoTest {

	public static void main(String[] args) throws InterruptedException, IOException {

		// TODO Auto-generated method stub
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\prudh\\eclipse-workspace\\demo\\src\\test\\java\\cscart\\demo\\inputdata.properties");
		prop.load(fis);
		
		BaseClass baseclass = new BaseClass();
		WebDriver firstini=baseclass.initializebrowser();
		LandingPage loginpage = new LandingPage( firstini);
		loginpage.goTo();
		loginpage.loginDemo(prop.getProperty("email"));
		SearchProductsPage searchproductspage = new SearchProductsPage(firstini);
		searchproductspage.searchproducts(prop.getProperty("productname"));
		searchproductspage.selectbrand(prop.getProperty("brandname"));
		//List<WebElement> products =searchproductspage.getProductsList();	
		searchproductspage.addAllProductsToCart();
		Boolean result=searchproductspage.verifycountofproducts();
	    Assert.assertTrue(result);
		searchproductspage.clickcheckout();	
		CheckoutPage checkoutpage = new CheckoutPage (firstini);
		checkoutpage.fillcustomerdetails(prop.getProperty("customeraddress"),prop.getProperty("customeremail") );
		checkoutpage.selectpayment(prop.getProperty("customerphone"));
		checkoutpage.placeorders();
		
		
	}

}


